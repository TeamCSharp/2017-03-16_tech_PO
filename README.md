% Anpassung an DB-Feldern: VarChar() ändern zu nVarChar()
% Beat Rohrer

# Anpassung an DB-Feldern: VarChar() ändern zu nVarChar()

# Allgemein

- Unicode ist kein grosses Latin-1, Unicode ist ein Versuch alle Schriften dieser Welt abzubilden
- wir speichern alle Namen unserer Studenten, Mitarbeiter, Dozenten und externer Kontakte in EventoNG – das System darf nicht nur in 80% der Fälle funktionieren

# Problem 1

## 

Dargestellte Unicode-Zeichen können aus mehreren einzelnen Zeichen zusammengesetzt sein

Ę̮̱͔͓ͯ͗ͫ̌̏ͫ͌́x̘̤͚̰̫̫̗̤̱̒̓ͨͯ͑̓ͥͫ̕å̰͚̓͒ͫm̛̤͕̫̳̺̩̄̓ͨͥ͜ͅp̰͉͗ͤl̵̖̗̫͍͓͋̍̐͌̐̒e̡̧͔̮̿͒͋̈́͡

## Demo

## Zahlen

- Wikidata-Liste aller Nachnamen: 78402
- Anzahl der Namen, welche aus zusammengesetzten Zeichen bestehen: 10969

## Folgen

- Texte werden nicht als gleich erkannt
- Zeichen werden beim Abschneiden verfälscht
- Da `tblUnicodeConversion`.`UCUnicodeZeichen` nur ein `NCHAR(1)` ist, wird z.B. aus dem ä ein a oder man kann es nicht speichern
- Felder mit Zeichenbegrenzungen werden falsch gezählt
- Nutzern kann nicht gesagt werden, welches Zeichen falsch ist

# Problem 2

## 

Namen kann man nicht einfach in unser Alphabet übersetzen, indem man gewisse Umlaute entfernt

## Erläuterung

- die gleichen Buchstaben werden in verschiedenen Sprachen anders ausgesprochen und müssen je nach Sprache deswegen auch anders in unseren Schriftsatz übertragen werden
- viele Buchstaben wechseln ihre Aussprache je nachdem, neben welchen anderen Buchstaben sie stehen (z.B. )

## Beispiel

- Französich: "Janine": das E am Ende ist lautlos

## Folgen

- Die Namen aus dem dem nicht-Unicode-Feld werden in allen Schnittstellen verwendet, die kein Unicode können. Bei uns z.B. die Campus-Karten
- da alles automatisch läuft, können Sekretariate die Schreibweise nicht auf Wunsch korrigieren

# 

## Vorschlag: Unicode-Felder als Zusatz

- wir führen zusätzliche Unicode-Felder ein, die ausgefüllt werden können, aber nicht müssen
- dieses wird in EventoNG angezeigt, die Sekretariate arbeiten jedoch mit dem bestehenden Feld
- ein "Computed Field" in der DB enthält das Unicode-Feld, wenn gesetzt, und sonst das nicht Unicode-Feld
- alle Eingaben werden normalisiert

## Vorteile

- alle Dokumente können die Sonderzeichen benutzen
- keine Änderungen an den Schnittstellen notwendig, wenn man Unicode nicht will
- Studenten können sagen, wie sie in unserem Alphabet geschrieben werden möchten
- keine Performance-Einbussen

# 

## Vorschlag: Überarbeiten

CLX überarbeitet bestehenden Vorschlag unter Berücksichtigung der erwähnten Eigenheiten von Unicode