void Main()
{
	string kopiert = "ändern";
	string geschrieben = "ändern";
	
	stringinfo(kopiert);
	//stringinfo(geschrieben);
	compare(kopiert, geschrieben);
	
	string kopiert_normalisiert = kopiert.Normalize(NormalizationForm.FormC);
	string geschrieben_normalisiert = geschrieben.Normalize(NormalizationForm.FormC);
	//compare(kopiert_normalisiert, kopiert_normalisiert);
}

void stringinfo(string a)
{
	Console.WriteLine("Normal");
	Console.WriteLine("Erstes Zeichen: " + a.Substring(0,1));
	Console.WriteLine("Anzahl Zeichen: " + a.Length);
	
	Console.WriteLine("\nGlobalization");
	System.Globalization.StringInfo b = new System.Globalization.StringInfo(a);
	Console.WriteLine("Erstes Zeichen: " + b.SubstringByTextElements(0, 1));
	Console.WriteLine("Anzahl Zeichen: " + b.LengthInTextElements);
}

void compare(string a, string b)
{
	Console.WriteLine("\nVergleich");
	if(a == b)
	{
		Console.WriteLine(a + " und " + b + " sind gleich");
	}
	else
	{
		Console.WriteLine(a + " und " + b + " sind nicht gleich");
	}
}

